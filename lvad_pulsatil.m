%% modelagem do sistema cardiovascular humano
% larissa artemis luna monteiro
% engenharia da computa��o

clc;
clear;

%% par�metros gerais

t_inicial = 0;
passo = 0.0001;
t_final = 3;

tempo = t_inicial:passo:t_final; % tempo da simula��o

E_min = 0.05; % elast�ncia m�nima
E_max = 3; % elast�ncia m�xima
freq_card = 60; % frequ�ncia card�aca
V_0 = 15; % volume de refer�ncia (em ml)

T = 60/freq_card; % intervalo do ciclo card�aco
T_max = 0.2 + 0.1555*T; % tempo m�ximo do ciclo card�aco
t_norm = tempo/T_max; % tempo normalizado

R_c = 0.0398; % resist�ncia caracter�stica
R_a = 0.0010; % resist�ncia da v�lvula aorta
R_s = 0.8738; % resist�ncia do sistema vascular
R_m = 0.0050; % resist�ncia da v�lvula mitral

C_ae = 4.4000; % complac�ncia arterial esquerda
C_ao = 0.0800; % complac�ncia da aorta
C_s = 2.896; % complac�ncia sist�mica

L_s = 0.001025; % qtde sangue que sangue que fica sempre no cora��o

D_a = 1; % "diodo" da v�lvula da aorta
D_m = 1; % "diodo" da v�lvula mitral
D_i = 1;
D_o = 1;

%% par�metros do MaMeMi

i = t_final / passo;

maxi = zeros(1,i);
mini = zeros(1,i);

a = zeros(1,i);
e  = zeros(1,i);
g = zeros(1,i);
h = zeros(1,i);
n = zeros(1,i);
r = zeros(1,i);
v = zeros(1,i);
w = zeros(1,i);

z_0 = zeros(1,i);
gammad = zeros(1,i);
deltaS = zeros(1,i);
R = zeros(1,i);
deltaPVAD = zeros(1,i);

xecg = zeros(i, 1);
yecg = zeros(i, 1);
zecg = zeros(i, 1);

deltaS(1) = 1;
xecg(1) = -1;

%% par�metros do LVAD puls�til

R_i = 0.15; % Resist�ncia de entrada
R_o = @(Q_o) 0.05 + 0.00015 * abs(Q_o); % Resist�ncia de sa�da
R_k = 0; % Resist�ncia � suc��o
R_p = 0.05; % Resist�ncia tor�xica
R_d = 0.01; % Resist�ncia da linha pneun�tica

L_i = 0.0854; % Inert�ncia de entrada
L_o = 0.0087; % Inert�ncia de sa�da
L_p = 0.0033; % Inert�ncia tor�xica

alpha_PVAD = 0.15; % par�metro PVAD (s/mL)

beta_0 = 0.17070; 
beta_1 = 0.02177; 
beta_2 = -9.3e-5; 

C_d = 4; % Linha pneum�tica
C_p = 2; % Linha tor�xica

Vd_vad = 107;
P_d = 0;

%% par�metros e matrizes  pra detec��o da onda R

atv = 0;
sigma = 2;
delta = 2;
beta = 15;

ip = 0;
lg = 0;

A_ecg = [1.2, -5, 30, -7.5, 0.75];
B_ecg = [0.25, 0.1, 0.1, 0.1, 0.4];

TH = [-1/3, -1/12, 0, 1/12, 1/2]*pi;

%% simula��o do MaMeMi

nLinhas = (t_final/passo) - 1;

for i = 1:nLinhas
    % algoritmo de integra��o Runge Kutta de 4� ordem
    k = 0;
    w_ecg = 2*pi/T;
    alpha = 1-sqrt(xecg(i)^2 + yecg(i)^2);
    th = atan2(yecg(i),xecg(i));
    for ii = 1:5
       k = k - (A_ecg(ii)*(th-TH(ii))*exp(-(th-TH(ii))^2/(2*B_ecg(ii)^2)));
    end

    A_i = [alpha, -w_ecg, 0; w_ecg, alpha, 0; 0, 0, -1];
    B_i = [0; 0; k + z_0(i)];

    dx = A_i * [xecg(i); yecg(i); zecg(i)] +  B_i;
    kx1 = passo * dx;
    x1 = [xecg(i); yecg(i); zecg(i)] + 0.5 * kx1;
       
    k = 0;
    alpha = 1-sqrt(x1(1)^2 + x1(2)^2);
    th = atan2(x1(2),x1(1));
    for ii = 1:5
       k = k - (A_ecg(ii)*(th-TH(ii))*exp(-(th-TH(ii))^2/(2*B_ecg(ii)^2)));
    end

    A_i = [alpha, -w_ecg, 0; w_ecg, alpha, 0; 0, 0, -1];
    B_i = [0; 0; k + z_0(i)];
    
    dx = A_i * x1 +  B_i;
    kx2 = passo * dx;
    x1 = [xecg(i); yecg(i); zecg(i)] + 0.5 * kx2;
    
    k = 0;
    alpha = 1-sqrt(x1(1)^2 + x1(2)^2);
    th = atan2(x1(2),x1(1));
    for ii = 1:5
       k = k - (A_ecg(ii)*(th-TH(ii))*exp(-(th-TH(ii))^2/(2*B_ecg(ii)^2)));
    end

    A_i = [alpha, -w_ecg, 0; w_ecg, alpha, 0; 0, 0, -1];
    B_i = [0; 0; k + z_0(i)];
    
    dx = A_i * x1 +  B_i;
    kx3 = passo * dx;
    x1 = [xecg(i); yecg(i); zecg(i)] + kx3;
    
    k = 0;
    alpha = 1-sqrt(x1(1)^2 + x1(2)^2);
    th = atan2(x1(2),x1(1));
    for ii = 1:5
       k = k - (A_ecg(ii)*(th-TH(ii))*exp(-(th-TH(ii))^2/(2*B_ecg(ii)^2)));
    end

    A_i = [alpha, -w_ecg, 0; w_ecg, alpha, 0; 0, 0, -1];
    B_i = [0; 0; k + z_0(i)];
    
    dx = A_i * x1 +  B_i;
    kx4 = passo * dx;
    
    xf = [xecg(i); yecg(i); zecg(i)] + (kx1 + 2 * kx2 + 2 * kx3 + kx4)/6;
 
    z_0(i+1) = 0.15*sin(2*pi*(60/(12+randn))*tempo(i+1));
    freq_card = 60 + 2*randn;
    T = 60/freq_card;

    xecg(i+1) = xf(1);
    yecg(i+1) = xf(2);
    zecg(i+1) = xf(3);

    if zecg(i+1) < mini(i) 
        mini(i+1) = mini(i) - sigma*delta;    
    elseif zecg(i+1) >= mini(i)
        mini(i+1) = mini(i) + delta;
    end
    
    if zecg(i+1) > maxi(i) 
        maxi(i+1) = maxi(i) + sigma*delta;    
    elseif zecg(i+1) <= maxi(i)
        maxi(i+1) = maxi(i) - delta;
    end

    a(i+1) = maxi(i+1)-mini(i+1);
    h(i+1) = zecg(i+1) - (maxi(i+1)+mini(i+1))/2;

    if a(i+1) <= h(i+1)
        n(i+1) = sign(h(i+1)*(abs(h(i+1))-a(i+1)));
    else
        n(i+1) = 0;
    end

    if i > beta
        if (n(i)>0) && (n(i)>n(i-beta)) && (n(i)>n(i+beta))   
            g(i) = n(i) - max(n(i-beta),n(i+beta));
        elseif (n(i)<0) && (n(i)<n(i-beta)) && (n(i)<n(i+beta))   
            g(i) = n(i) + min(n(i-beta),n(i+beta));
        else
            g(i) = 0;
        end

        if(g(i)>g(i-1) && g(i)>g(i+1))
            r(i) = g(i);
        else
            r(i) = 0;
        end

        if(g(i)<g(i-1) && g(i)<g(i+1))
            v(i) = g(i);
        else
            v(i) = 0;
        end
    end
    if r(i)>0
        w(i) = r(i);
    end
    
    if v(i)<0
        w(i) = -v(i);
    end

    if(w(i) == 1)
        atv = 1;
    end

    if ((atv ==1) && (zecg(i)>=0.03))
        atv = 2;
    end

    R(i) = 0;
    if ((atv ==2) && (zecg(i)<=0.03))
        j = 1;
        aux_E = 1;
        atv = 0;
        R(i) = 1;
    end
end

%% resultados do MaMeMi 

% plot(tempo(1:end-1),zecg);
% hold on;
% plot(tempo(1:end-1),max(zecg)*R_dtct,'k');

r_wave_times = find(max(zecg)*R) * passo;

%% fun��es e matrizes do Simaan usando a elast�ncia em vez do tempo

% elast�ncia normalizada (double hill)
E_n = @(t_n)1.55*((t_n/0.7).^(1.9))./((1 + (t_n/0.7).^1.9))*1./(1 + (t_n./1.17).^21.9);

E_t = @(t) (t > r_wave_times(1))*(E_max - E_min) * E_n(mod(t, r_wave_times(abs(t - r_wave_times) == min(abs(t - r_wave_times))))/T_max) + E_min;

% press�o no ventr�culo esquerdo
P_ve_function = @(E_t2, V_ve) E_t2*(V_ve - V_0);

A = @(E_t2, D_m, D_a, beta_i, beta_o, gamma, omega_i_t, omega_o_t) [-(D_m/R_m + D_a/R_a)*E_t2, D_m/R_m, 0, D_a/R_a, 0, -1, 0, 0, 0 ;
    (D_m * E_t2)/(R_m * C_ae), -(C_ae^-1)*(1/R_s + D_m/R_m), 0, 0, (R_s*C_ae)^-1, 0, 0, 0, 0;
    0, 0, -R_c/L_s, L_s^-1, -L_s^-1, 0, 0, 0, 0;
    (D_a * E_t2)/(R_a*C_ao), 0, -C_ao^-1, -D_a/(R_a*C_ao), 0, 0, C_ao^-1, 0, 0;
    0, (R_s*C_s)^-1, C_s^-1, 0, -(R_s*C_s)^-1, 0, 0, 0, 0;
    beta_i * E_t2 / (1 - gamma * L_p), 0, 0, - gamma / (1 - gamma * L_p), 0, - (alpha_PVAD * (beta_i - gamma) + (beta_i * R_p + omega_i_t + gamma * R_p))/(1 - gamma * L_p), (alpha_PVAD * (beta_i - gamma) + (beta_i * R_p - gamma * R_p - beta_i * L_p * omega_o_t))/(1 - gamma * L_p), -(beta_i - gamma)/(1 - gamma * L_p), -(beta_i - gamma)/(C_p * (1 - gamma * L_p));
    0, 0, 0, -beta_o / (1 - gamma * L_p), 0, (alpha_PVAD * (beta_o - gamma) + (beta_o * R_p - beta_o * L_p * omega_i_t - gamma * R_p))/(1 - gamma * L_p), -(alpha_PVAD * (beta_o - gamma) + (beta_o * R_p + omega_o_t - gamma * R_p))/(1 - gamma * L_p), (beta_o - gamma) / (1 - gamma * L_p), (beta_o - gamma)/(C_p * (1 - gamma * L_p));
    0, 0, 0, 0, 0, 0, 0, -(R_d * C_d)^-1, 0;
    0, 0, 0, 0, 0, 1, -1, 0, 0];

p = @(E_t2, D_m, D_a, beta_i, beta_o, gamma) [(D_m/R_m + D_a/R_a)*E_t2*V_0;
    -(D_m*E_t2*V_0)/(R_m*C_ae);
    0;
    -(D_a*E_t2*V_0)/(R_a*C_ao);
    0;
    ((beta_i - gamma)/C_p) * Vd_vad - beta_i * E_t2 * V_0;
    ((beta_o - gamma)/C_p) * Vd_vad;
    P_d / (R_d * C_d);
    0];

%% vari�vel de controle

w = @(t) 12000 + 100 * t;

%% matrizes do modelo e suas condi��es iniciais do Simaan adicionando a elast�ncia

nLinhas = t_final/passo;

E = zeros(nLinhas, 1);
E(1) = E_t(0);

omega = zeros(nLinhas, 1);
omega(1) = w(0);

omega_i = zeros(nLinhas, 1);

omega_o = zeros(nLinhas, 1);

P_ae = zeros(nLinhas, 1);
P_ae(1) = 5;

Q_a = zeros(nLinhas, 1);
Q_a(1) = 0;

P_ao = zeros(nLinhas, 1);
P_ao(1) = 90;

P_s = zeros(nLinhas, 1);
P_s(1) = 90;

P_ex = zeros(nLinhas, 1);

Q_b = zeros(nLinhas, 1);
Q_b(1) = 0;

Q_i = zeros(nLinhas, 1);

Q_o = zeros(nLinhas, 1);

V_ve = zeros(nLinhas, 1);
V_ve(1) = 140;

V_c = zeros(nLinhas, 1);

P_ve = zeros(nLinhas, 1);
P_ve(1) = P_ve_function(E(1), V_ve(1));

%% Simula��o do Simaan usando a elast�ncia em vez do tempo

t = 0;
n = 1;

while t < t_final
    t = t + passo;
    
    % Valores dos diodos referente �s v�lvulas do <3 de acordo com
    % TABLE II PHASES OF THE CARDIAC CYCLE
    if (P_ae(n) > P_ve(n)) % "enchendo"
        D_m = 1;
        D_a = 0;
    elseif (P_ve(n) > P_ao(n)) % "esvaziando"
        D_m = 0;
        D_a = 1;
    else % relaxamento / contra��o isovolumetrica
        D_m = 0;
        D_a = 0;
    end % D_m = D_a = 1 n�o � uma condi��o poss�vel
    
    x = [V_ve(n); P_ae(n); Q_a(n); P_ao(n); P_s(n); Q_i(n); Q_o(n); P_ex(n); V_c(n)];
    
    beta_i = D_i/(L_i + D_i * L_p);		
    beta_o = D_o/(L_o + D_o * L_p);		
    gamma = beta_i * L_p * beta_o;		
    omega_i_t = R_i * (L_i + D_i * L_p);		
    omega_o_t = R_o(Q_o(n)) * (L_o + D_o * L_p);		
        		
    % RK4 integration		
    A_i = A(E(n), D_m, D_a, beta_i, beta_o, gamma, omega_i_t, omega_o_t);		
    p_i = p(E(n), D_m, D_a, beta_i, beta_o, gamma);		
    		    
    % algoritmo de integra��o Runge Kutta de 4� ordem  
    dx =  A_i * x +  p_i;
    kx1 = passo * dx;
    x1 = x + 0.5 * kx1;
    
    dx = A_i * x1 +  p_i;
    kx2 = passo * dx;
    x1 = x + 0.5 * kx2;
    
    dx = A_i * x1 +  p_i;
    kx3 = passo * dx;
    x1 = x + kx3;
    
    dx = A_i * x1 +  p_i;
    kx4 = passo * dx;
    
    xf = x + (kx1 + 2 * kx2 + 2 * kx3 + kx4)/6;
    
    n = n + 1;
    
    % salvando os dados
    V_ve(n) = xf(1);
    P_ae(n) = xf(2);
    Q_a(n) = xf(3);
    P_ao(n) = xf(4);
    P_s(n) = xf(5);
    Q_i(n) = xf(6);
    Q_o(n) = xf(7);
    P_ex(n) = xf(8);
    V_c(n) = xf(9);
    
    E(n) = E_t(t);
    omega_i(n) = omega_i_t;
    omega_o(n) = omega_o_t;
    
    P_ve(n) = P_ve_function(E(n), V_ve(n));
end

%% Gr�ficos ainda mais novos!

figure(1);
subplot(3, 1, 1);
plot(tempo, P_ao);
hold on;
plot(tempo, P_ve);
hold on;
plot(tempo, P_ae);

grid on;
xlabel('Tempo (s)');
ylabel('Press�o (mmHg)');

subplot(3, 1, 2);
plot(tempo, Q_a);

ymin = min(Q_a) - 100;
ymax = max(Q_a) + 100;
axis([0, t_final, ymin, ymax]);

grid on;
xlabel('Tempo (s)');
ylabel('Fluxo na aorta (ml/s)');

subplot(3, 1, 3);
plot(tempo, V_ve);

ymin = min(V_ve) - 20;
ymax = max(V_ve) + 20;
axis([0, t_final, ymin, ymax]);

grid on;
xlabel('Tempo (s)');
ylabel('LVV (ml)');

figure(2);
subplot(2, 1, 1);
plot(tempo(1:end-1), zecg);
hold on;
plot(tempo(1:end-1),max(zecg)*R,'k');

ymin = min(min(zecg)*R)-0.01;
ymax = max(max(zecg)*R)+0.01;
axis([0, t_final, ymin, ymax]);
grid on;

xlabel('Tempo (s)');
ylabel('Detec��o de Onda R');
legend('ECG', 'MaMeMi');
 
subplot(2, 1, 2);
plot(tempo(1:end),E(1:end));
hold on;
plot(tempo(1:end-1),40*max(zecg)*R,'k');

ymin = min(40*max(zecg)*R)-0.1;
ymax = max(E(1:end))+0.1;
axis([0, t_final, ymin, ymax]);
grid on;

xlabel('Tempo (s)');
ylabel('Elast�ncia');
legend('E(t)', 'MaMeMi');
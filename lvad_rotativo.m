%% modelagem do sistema cardiovascular humano
% larissa artemis luna monteiro
% engenharia da computa��o

clc;
clear all;

%% par�metros gerais

t_inicial = 0;
passo = 0.0001;
t_final = 60;

tempo = t_inicial:passo:t_final; % tempo da simula��o

E_min = 0.05; % elast�ncia m�nima
E_max = 2; % elast�ncia m�xima
freq_card = 60; % frequ�ncia card�aca
V_0 = 10; % volume de refer�ncia (em ml)

T = 60/freq_card; % intervalo do ciclo card�aco
T_max = 0.2 + 0.1555*T; % tempo m�ximo do ciclo card�aco
t_norm = tempo/T_max; % tempo normalizado

R_c = 0.0398; % resist�ncia caracter�stica
R_a = 0.0010; % resist�ncia da v�lvula aorta
R_s = 1.0000; % resist�ncia do sistema vascular
R_m = 0.0050; % resist�ncia da v�lvula mitral

C_ae = 4.4000; % complac�ncia arterial esquerda
C_ao = 0.0800; % complac�ncia da aorta
C_s = 1.3300; % complac�ncia sist�mica

L_s = 0.0005; % qtde sangue que sangue que fica sempre no cora��o

D_a = 1; % "diodo" da v�lvula da aorta
D_m = 1; % "diodo" da v�lvula mitral

%% par�metros do MaMeMi

i = t_final / passo;

maxi = zeros(1,i);
mini = zeros(1,i);

a = zeros(1,i);
e  = zeros(1,i);
g = zeros(1,i);
h = zeros(1,i);
n = zeros(1,i);
r = zeros(1,i);
v = zeros(1,i);
w = zeros(1,i);

z_0 = zeros(1,i);
gammad = zeros(1,i);
deltaS = zeros(1,i);
R = zeros(1,i);
deltaPVAD = zeros(1,i);

xecg = zeros(i, 1);
yecg = zeros(i, 1);
zecg = zeros(i, 1);

deltaS(1) = 1;
xecg(1) = -1;

%% par�metros do LVAD 

R_i = 0.0677; % Resist�ncia de entrada
R_o = 0.0677; % Resist�ncia de sa�da
R_k = 0; % Resist�ncia � suc��o

L_i = 0.0127; % Inert�ncia de entrada
L_o = 0.0127; % Inert�ncia de sa�da

beta_0 = 0.17070; 
beta_1 = 0.02177; 
beta_2 = -9.3e-5; 

%% par�metros e matrizes  pra detec��o da onda R

atv = 0;
beta = 15;
delta = 2;
sigma = 2;

A_ecg = [1.2, -5, 30, -7.5, 0.75];
B_ecg = [0.25, 0.1, 0.1, 0.1, 0.4];

TH = [-1/3, -1/12, 0, 1/12, 1/2]*pi;

%% simula��o do MaMeMi

nLinhas = (t_final/passo) - 1;

for i = 1:nLinhas
    % algoritmo de integra��o Runge Kutta de 4� ordem
    k = 0;
    w_ecg = 2*pi/T;
    alpha = 1-sqrt(xecg(i)^2 + yecg(i)^2);
    th = atan2(yecg(i),xecg(i));
    for ii = 1:5
       k = k - (A_ecg(ii)*(th-TH(ii))*exp(-(th-TH(ii))^2/(2*B_ecg(ii)^2)));
    end

    A_i = [alpha, -w_ecg, 0; w_ecg, alpha, 0; 0, 0, -1];
    B_i = [0; 0; k + z_0(i)];

    dx = A_i * [xecg(i); yecg(i); zecg(i)] +  B_i;
    kx1 = passo * dx;
    x1 = [xecg(i); yecg(i); zecg(i)] + 0.5 * kx1;
       
    k = 0;
    alpha = 1-sqrt(x1(1)^2 + x1(2)^2);
    th = atan2(x1(2),x1(1));
    for ii = 1:5
       k = k - (A_ecg(ii)*(th-TH(ii))*exp(-(th-TH(ii))^2/(2*B_ecg(ii)^2)));
    end

    A_i = [alpha, -w_ecg, 0; w_ecg, alpha, 0; 0, 0, -1];
    B_i = [0; 0; k + z_0(i)];
    
    dx = A_i * x1 +  B_i;
    kx2 = passo * dx;
    x1 = [xecg(i); yecg(i); zecg(i)] + 0.5 * kx2;
    
    k = 0;
    alpha = 1-sqrt(x1(1)^2 + x1(2)^2);
    th = atan2(x1(2),x1(1));
    for ii = 1:5
       k = k - (A_ecg(ii)*(th-TH(ii))*exp(-(th-TH(ii))^2/(2*B_ecg(ii)^2)));
    end

    A_i = [alpha, -w_ecg, 0; w_ecg, alpha, 0; 0, 0, -1];
    B_i = [0; 0; k + z_0(i)];
    
    dx = A_i * x1 +  B_i;
    kx3 = passo * dx;
    x1 = [xecg(i); yecg(i); zecg(i)] + kx3;
    
    k = 0;
    alpha = 1-sqrt(x1(1)^2 + x1(2)^2);
    th = atan2(x1(2),x1(1));
    for ii = 1:5
       k = k - (A_ecg(ii)*(th-TH(ii))*exp(-(th-TH(ii))^2/(2*B_ecg(ii)^2)));
    end

    A_i = [alpha, -w_ecg, 0; w_ecg, alpha, 0; 0, 0, -1];
    B_i = [0; 0; k + z_0(i)];
    
    dx = A_i * x1 +  B_i;
    kx4 = passo * dx;
    
    xf = [xecg(i); yecg(i); zecg(i)] + (kx1 + 2 * kx2 + 2 * kx3 + kx4)/6;
 
    z_0(i+1) = 0.15*sin(2*pi*(60/(12+randn))*tempo(i+1));
    freq_card = 60 + 2*randn;
    T = 60/freq_card;

    xecg(i+1) = xf(1);
    yecg(i+1) = xf(2);
    zecg(i+1) = xf(3);

    if zecg(i+1) < mini(i) 
        mini(i+1) = mini(i) - sigma*delta;    
    elseif zecg(i+1) >= mini(i)
        mini(i+1) = mini(i) + delta;
    end
    
    if zecg(i+1) > maxi(i) 
        maxi(i+1) = maxi(i) + sigma*delta;    
    elseif zecg(i+1) <= maxi(i)
        maxi(i+1) = maxi(i) - delta;
    end

    a(i+1) = maxi(i+1)-mini(i+1);
    h(i+1) = zecg(i+1) - (maxi(i+1)+mini(i+1))/2;

    if a(i+1) <= h(i+1)
        n(i+1) = sign(h(i+1)*(abs(h(i+1))-a(i+1)));
    else
        n(i+1) = 0;
    end

    if i > beta
        if (n(i)>0) && (n(i)>n(i-beta)) && (n(i)>n(i+beta))   
            g(i) = n(i) - max(n(i-beta),n(i+beta));
        elseif (n(i)<0) && (n(i)<n(i-beta)) && (n(i)<n(i+beta))   
            g(i) = n(i) + min(n(i-beta),n(i+beta));
        else
            g(i) = 0;
        end

        if(g(i)>g(i-1) && g(i)>g(i+1))
            r(i) = g(i);
        else
            r(i) = 0;
        end

        if(g(i)<g(i-1) && g(i)<g(i+1))
            v(i) = g(i);
        else
            v(i) = 0;
        end
    end
    if r(i)>0
        w(i) = r(i);
    end
    
    if v(i)<0
        w(i) = -v(i);
    end

    if(w(i) == 1)
        atv = 1;
    end

    if ((atv ==1) && (zecg(i)>=0.03))
        atv = 2;
    end

    R(i) = 0;
    if ((atv ==2) && (zecg(i)<=0.03))
        j = 1;
        aux_E = 1;
        atv = 0;
        R(i) = 1;
    end
end

%% resultados do MaMeMi 

% plot(tempo(1:end-1),zecg);
% hold on;
% plot(tempo(1:end-1),max(zecg)*R_dtct,'k');

r_wave_times = find(max(zecg)*R) * passo;

%% fun��es e matrizes do Simaan usando a elast�ncia em vez do tempo

% elast�ncia normalizada (double hill)
E_n = @(t_n)1.55*((t_n/0.7).^(1.9))./((1 + (t_n/0.7).^1.9))*1./(1 + (t_n./1.17).^21.9);

E_t = @(t) (t > r_wave_times(1))*(E_max - E_min) * E_n(mod(t, r_wave_times(abs(t - r_wave_times) == min(abs(t - r_wave_times))))/T_max) + E_min;

% press�o no ventr�culo esquerdo
P_ve_function = @(E_t2, V_ve) E_t2*(V_ve - V_0);

A = @(E_t2, D_m, D_a, R_k) [-(D_m/R_m + D_a/R_a)*E_t2, D_m/R_m, 0, D_a/R_a, 0, -1 ;
    (D_m * E_t2)/(R_m * C_ae), -(C_ae^-1)*(1/R_s + D_m/R_m), 0, 0, (R_s*C_ae)^-1, 0;
    0, 0, -R_c/L_s, L_s^-1, -L_s^-1, 0;
    (D_a * E_t2)/(R_a*C_ao), 0, -C_ao^-1, -D_a/(R_a*C_ao), 0, C_ao^-1;
    0, (R_s*C_s)^-1, C_s^-1, 0, -(R_s*C_s)^-1, 0;
    E_t2/(L_i + L_o + beta_1), 0, 0, -(L_i + L_o + beta_1)^-1, 0, -(beta_0 + R_i + R_k + R_o)/(L_i + L_o + beta_1)];

p = @(E_t2, D_m, D_a) [(D_m/R_m + D_a/R_a)*E_t2*V_0;
    -(D_m*E_t2*V_0)/(R_m*C_ae);
    0;
    -(D_a*E_t2*V_0)/(R_a*C_ao);
    0;
    -(E_t2 * V_0)/(L_i + L_o + beta_1)];

B = [0;		
    0;		
    0;		
    0;		
    0;		
    -beta_2/(L_i + L_o + beta_1)];

%% vari�vel de controle

w = @(t) 12000 + 100 * t;

%% matrizes do modelo e suas condi��es iniciais do Simaan adicionando a elast�ncia

nLinhas = t_final/passo;

V_ve = zeros(nLinhas, 1);
V_ve(1) = 140;

P_ae = zeros(nLinhas, 1);
P_ae(1) = 5;

Q_a = zeros(nLinhas, 1);
Q_a(1) = 0;

P_ao = zeros(nLinhas, 1);
P_ao(1) = 90;

P_s = zeros(nLinhas, 1);
P_s(1) = 90;

P_ve = zeros(nLinhas, 1);
P_ve(1) = P_ve_function(E_t(0), V_ve(1));

E = zeros(nLinhas, 1);
E(1) = E_t(0);

Q_b = zeros(nLinhas, 1);
Q_b(1) = 0;

omega = zeros(nLinhas, 1);
omega(1) = w(0);

%% Simula��o do Simaan usando a elast�ncia em vez do tempo

t = 0;
n = 1;

while t < t_final
    t = t + passo;
    
    % Valores dos diodos referente �s v�lvulas do <3 de acordo com
    % TABLE II PHASES OF THE CARDIAC CYCLE
    if (P_ae(n) > P_ve(n)) % "enchendo"
        D_m = 1;
        D_a = 0;
    elseif (P_ve(n) > P_ao(n)) % "esvaziando"
        D_m = 0;
        D_a = 1;
    else % relaxamento / contra��o isovolumetrica
        D_m = 0;
        D_a = 0;
    end % D_m = D_a = 1 n�o � uma condi��o poss�vel
    
    x = [V_ve(n); P_ae(n); Q_a(n); P_ao(n); P_s(n); Q_b(n)];
    
    % c�culo da vari�vel de controle e atualiza��o da resist�ncia
    u = (w(t) * 2 * pi / 60)^2; 
    R_k = (P_ve(n) <= 1) * (-3.5 * (P_ve(n) - 1));

    % algoritmo de integra��o Runge Kutta de 4� ordem  
    dx = A(E(n), D_m, D_a, R_k) * x +  p(E(n), D_m, D_a) + B * u;
    kx1 = passo * dx;
    x1 = x + 0.5 * kx1;
    
    dx = A(E(n), D_m, D_a, R_k) * x1 +  p(E(n), D_m, D_a) + B * u;
    kx2 = passo * dx;
    x1 = x + 0.5 * kx2;
    
    dx = A(E(n), D_m, D_a, R_k) * x1 +  p(E(n), D_m, D_a) + B * u;
    kx3 = passo * dx;
    x1 = x + kx3;
    
    dx = A(E(n), D_m, D_a, R_k) * x1 +  p(E(n), D_m, D_a) + B * u;
    kx4 = passo * dx;
    
    xf = x + (kx1 + 2 * kx2 + 2 * kx3 + kx4)/6;
    
    n = n + 1;
    
    % salvando os dados
    V_ve(n) = xf(1);
    P_ae(n) = xf(2);
    Q_a(n) = xf(3);
    P_ao(n) = xf(4);
    P_s(n) = xf(5);
    Q_b(n) = xf(6);
    
    E(n) = E_t(t);
    omega(n) = w(t);
    
    P_ve(n) = P_ve_function(E(n), V_ve(n));
end

%% Gr�ficos novos!

figure(1);
plot(tempo, omega * 1e-3);
hold on;

ymin = min(omega * 1e-3);
ymax = max(omega * 1e-3);
axis([0, t_final, ymin, ymax]);

grid on;
xlabel('Tempo (s)');
ylabel('Velocidade (krpm)');
title('Pump Speed');

figure(2);
plot(tempo, Q_b);

ymin = min(Q_b)-10;
ymax = max(Q_b)+10;
axis([0, t_final, ymin, ymax]);

grid on;
xlabel('Tempo (s)');
ylabel('Fluxo (ml/s)');
title('Pump Flow');

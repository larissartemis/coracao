%% modelagem do sistema cardiovascular humano
% larissa artemis luna monteiro
% engenharia da computa��o

clc;
clear all;

%% par�metros

t_inicial = 0;
passo = 0.0001;
t_final = 3;

tempo = t_inicial:passo:t_final; % tempo da simula��o

E_min = 0.06; % elast�ncia m�nima
E_max = 2; % elast�ncia m�xima
freq_card = 75; % frequ�ncia card�aca
V_0 = 10; % volume de refer�ncia (em ml)

T = 60/freq_card; % intervalo do ciclo card�aco
T_max = 0.2 + 0.15*T; % tempo m�ximo do ciclo card�aco
t_norm = tempo/T_max; % tempo normalizado

R_c = 0.0398; % resist�ncia caracter�stica
R_a = 0.0010; % resist�ncia da v�lvula aorta
R_s = 1.0000; % resist�ncia do sistema vascular
R_m = 0.0050; % resist�ncia da v�lvula mitral

C_ae = 4.4000; % complac�ncia arterial esquerda
C_ao = 0.0800; % complac�ncia da aorta
C_s = 1.3300; % complac�ncia sist�mica

L_s = 0.0005; % qtde sangue que sangue que fica sempre no cora��o

D_a = 1; % "diodo" da v�lvula da aorta
D_m = 1; % "diodo" da v�lvula mitral

%% fun��es e matrizes 

% elast�ncia normalizada (double hill)
E_n = @(t_n)1.55*((t_n/0.7).^(1.9))./((1 + (t_n/0.7).^1.9))*1./(1 + (t_n./1.17).^21.9);
E_t = @(t)(E_max - E_min)*E_n(mod(t, T)/T_max) + E_min;

% press�o no ventr�culo esquerdo
P_ve_function = @(t, V_ve) E_t(t)*(V_ve - V_0);

A = @(t, D_m, D_a) [-(D_m/R_m + D_a/R_a)*E_t(t), D_m/R_m, 0, D_a/R_a, 0 ;
    (D_m * E_t(t))/(R_m * C_ae), -(C_ae^-1)*(1/R_s + D_m/R_m), 0, 0, (R_s*C_ae)^-1;
    0, 0, -R_c/L_s, L_s^-1, -L_s^-1;
    (D_a * E_t(t))/(R_a*C_ao), 0, -C_ao^-1, -D_a/(R_a*C_ao), 0;
    0, (R_s*C_s)^-1, C_s^-1, 0, -(R_s*C_s)^-1];

p = @(t, D_m, D_a) [(D_m/R_m + D_a/R_a)*E_t(t)*V_0;
    -(D_m*E_t(t)*V_0)/(R_m*C_ae);
    0;
    -(D_a*E_t(t)*V_0)/(R_a*C_ao);
    0];

%% matrizes do modelo e suas condi��es iniciais

nLinhas = t_final/passo;

V_ve = zeros(nLinhas, 1);
V_ve(1) = 140;

P_ae = zeros(nLinhas, 1);
P_ae(1) = 5;

Q_a = zeros(nLinhas, 1);
Q_a(1) = 0;

P_ao = zeros(nLinhas, 1);
P_ao(1) = 90;

P_s = zeros(nLinhas, 1);
P_s(1) = 90;

P_ve = zeros(nLinhas, 1);
P_ve(1) = P_ve_function(0, V_ve(1));

%% Simula��o 

t = 0;
n = 1;

while t < t_final
    t = t + passo;
    
    % Valores dos diodos referente �s v�lvulas do <3 de acordo com
    % TABLE II PHASES OF THE CARDIAC CYCLE
    if (P_ae(n) > P_ve(n)) % "enchendo"
        D_m = 1;
        D_a = 0;
    elseif (P_ve(n) > P_ao(n)) % "esvaziando"
        D_m = 0;
        D_a = 1;
    else % relaxamento / contra��o isovolumetrica
        D_m = 0;
        D_a = 0;
    end % D_m = D_a = 1 n�o � uma condi��o poss�vel
    
    x = [V_ve(n); P_ae(n); Q_a(n); P_ao(n); P_s(n)];
    
    % algoritmo de integra��o Runge Kutta de 4� ordem
    dx = A(t, D_m, D_a) * x +  p(t, D_m, D_a);
    kx1 = passo * dx;
    x1 = x + 0.5*kx1;
    
    dx = A(t, D_m, D_a) * x1 +  p(t, D_m, D_a);
    kx2 = passo * dx;
    x1 = x + 0.5*kx2;
    
    dx = A(t, D_m, D_a) * x1 +  p(t, D_m, D_a);
    kx3 = passo * dx;
    x1 = x + kx3;
    
    dx = A(t, D_m, D_a) * x1 +  p(t, D_m, D_a);
    kx4 = passo * dx;
    
    xf = x + (kx1 + 2 * kx2 + 2 * kx3 + kx4)/6;
    
    % colocando os dados no lugar correto
    V_ve(n+1) = xf(1);
    P_ae(n+1) = xf(2);
    Q_a(n+1) = xf(3);
    P_ao(n+1) = xf(4);
    P_s(n+1) = xf(5);
    P_ve(n+1) = P_ve_function(t, V_ve(n+1));
    
    n = n + 1;
end

%% Gr�ficos!

% subplot 1 - press�o 
subplot(3,1,1);
plot(tempo, P_ao);
hold on;
plot(tempo, P_ve, ':r');
hold on;
plot(tempo, P_ae, '--k');

axis([0, 3, -5, 150]);
grid on;

xlabel('tempo (s)');
ylabel('Press�o (mmHg)');
legend('AoP', 'LVP', 'LAP');

% subplot 2 - fluxo
subplot(3,1,2);
plot(tempo, Q_a);

axis([0, 3, -50, 700]);
grid on;

xlabel('tempo (s)');
ylabel('Fluxo na Aorta (ml/s)');
 
% subplot 3 - volume
subplot(3,1,3);
plot(tempo, V_ve);

axis([0, 3, 50, 160]);
grid on;

xlabel('tempo (s)');
ylabel('LVV (ml)');